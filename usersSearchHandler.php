<!DOCTYPE>
<html>
<body>

<?php 
	//set up
	include 'conn.php';
	$filter = $_GET["filter"];
?>
	
	<hr>
	<h2>Users</h2>
	<br><br>
	
	<!-- the table that lists the users -->
	<table style="margin: 0px auto;" class="table table-hover">
		<thead>
			<tr>
				<th>Username</th>
				<th>First Name</th>
				<th>Last Name</th>
			</tr>
		</thead>
		
	<?php 
		//query to get data from database
		$query = "SELECT c.customer_id, c.first_name, c.last_name FROM customer AS c WHERE c.customer_id LIKE '%$filter%' or c.first_name LIKE '%$filter%' or c.last_name LIKE '%$filter%'";
		$result = mysqli_query($con, $query);
		
			while($row = mysqli_fetch_array($result)) {
				$user = $row['customer_id'];
				$first = $row['first_name'];
				$last = $row['last_name'];
				
				echo "<tr align='left'>";
				echo "<td>$user</td>";
				echo "<td>$first</td>";
				echo "<td>$last</td>";
				
				//displays buttons for updating and deleting users
				echo "<td><a href='account.php?user=$user' class='btn btn-info btnmd'>Edit Account</a></td>";
				echo "<br>";
				echo "</tr>";
			}
			
			mysqli_close($con);
	?>
	</table>
</body>
</html>
