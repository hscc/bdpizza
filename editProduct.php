<?php
	session_start();
	include 'header.php';
	include 'conn.php';
?>
<body>
<?php
	//if admin deleted a product
	if (isset($_GET['product_id'])) {
		$prod_id = $_GET['product_id'];
		
		$query = "DELETE FROM product WHERE product_id = '$prod_id'";
		mysqli_query($con, $query);
		
		//tells admin what happened after they clicked delete
		$prod_name = $_GET['product_name'];
		$_SESSION['EP_message'] = "\"$prod_name\" has been deleted from products.";
	}
	
	if($_SESSION['admin'] != true) {
		header("Location: index.php");
	}
	
	//tells admin what happend
	if(isset($_SESSION['EP_message'])) {
		$EP_message= $_SESSION['EP_message'];
		echo "<div class='alert alert-success' align='center'>";
		echo "<b>$EP_message</b>";
		echo "</div>";
		unset($_SESSION['EP_message']);
	}
?>

<hr>
<h2>Edit Products</h2>
<br><br>

<table style="margin: 0px auto;" class="table table-hover">
	<thead>
		<tr>
			<th>Product Type</th>
			<th>Product Name</th>
			<th>Size</th>
			<th>Price</th>
		</tr>
	</thead>
	
	<?php
		$query = "SELECT p.product_id, pt.product_type_id, p.product_name, p.size, p.price FROM product p, product_type pt where p.product_type_id = pt.product_type_id";
		$result = mysqli_query($con, $query);
			
		while ($row = mysqli_fetch_array($result)) {
			$product_name = $row['product_name'];
			$size = $row['size'];
			$price = $row['price'];
			$product_id = $row['product_id'];
			$prod_type_id1 = $row['product_type_id'];	//the type id of a specific product
			
			echo "<form class='button' method='post' action='prodUpdateHandler.php'>";
			echo "<tr>";
			
			//product type stuff
			echo "<td>";
			echo "<select class='form-control' name='prod_type_id'>";

			$query2 = "select product_type_name, product_type_id from product_type";
			$result2 = mysqli_query($con, $query2);
			while($row2 = mysqli_fetch_array($result2)) {
				$product_type_name= $row2['product_type_name'];
				$prod_type_id2 = $row2['product_type_id'];	//all of the type id's
				
				if ($prod_type_id2 == $prod_type_id1) {
					echo "<option value='$prod_type_id2' selected='selected'>"; 
					echo "$product_type_name</option>"; 
				}
				else {
					echo "<option value='$prod_type_id2'>";
					echo "$product_type_name</option>"; 
				}
			}
			echo "</select>";
			echo "</td>";
			
			echo "<td><input type='text' class='form-control' name='product_name' value='$product_name' ></td>";
			echo "<td><input type='text' class='form-control' name='size' value='$size' ></td>";
			echo "<td><input type='number' class='form-control' name='price' value='$price' ></td>";
			echo "<input type='hidden' class='form-control' name='product_id' value='$product_id'>";
			echo "<td><button class='btn btn-info btnmd' type='submit'>Update</button></td>";
			echo "<td><a href='editProduct.php?product_id=$product_id&product_name=$product_name' class='btn btn-danger btnmd'>Delete</a></td>";
			echo "</tr>";
			echo "</form>";
		}
	?>
</table>

<?php include 'footer.php'; ?>
</html>
