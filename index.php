<?php
	session_start();
	include 'header.php';
	include 'conn.php';

//say that the login info is incorrect when user failed to login
	if(isset($_SESSION['login_fail'])) {
		$login_fail = $_SESSION['login_fail'];
		echo "<div class='alert alert-danger' align='center'>";
		echo "<b>$login_fail</b>";
		echo "</div>";
		unset($_SESSION['login_fail']);
	}
?>

<hr>
<h2>Login</h2>
<br><br>
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<form action="loginHandler.php" method=POST>
			<div class="form-group" align="left">
				<label for="user">Username:</label>
				<input type="text" class="form-control" name="user" placeholder="Enter username">
			</div>
			<div class="form-group" align="left">
				<label for="pass">Password:</label>
				<input type="password" class="form-control" name="pass" placeholder="Enter password">
			</div>
			<button type="submit" class="btn btn-primary btn-block" name="login">Login</button>
			<button type="submit" class="btn btn-danger btn-block" name="signup" formaction="signup.php">Sign Up</button>
		</form>
	</div>
	<div class="col-sm-4"></div>
</div>

<br><br>

<div class='row'>
	<div class='carousel-container'>
		<div class="carousel slide" id="myCarousel" data-ride="carousel">
			<ol class="carousel-indicators">
				<li class="active" data-target="#myCarousel" data-slide-to="0"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
			</ol>
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img class='cimg' alt="Meat Pizza" src="images/Pizza1.jpg">
				</div>
				<div class="carousel-caption">
					<h3>Delicious and 100% edible.</h3>
				</div>
				<div class="item">
					<img class='cimg' alt="Veggie Pizza" src="images/Pizza2.jpg">
				</div>
			</div>
		</div>
		<a class="left carousel-control" role="button" href="#myCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" role="button" href="#myCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
	
<?php include 'footer.php'; ?>
