<?php
session_start();
include 'conn.php';
include 'header.php';
if (isset($_SESSION['admin'])){
	echo "<div align='center'>";
	echo "<h2>Welcome Master Bruce.</h2><br>";
	echo "</div>";
}
else {
	echo "<div align='center'>";
	echo "<h2>Welcome " . $_SESSION['user'] . ".</h2><br>";
	echo "<form action='menu.php' method='POST'>";
	echo "<button class='btn btn-info btnmd' type='submit'>Make an Order</button>";
	echo "</form>";
	echo "</div>";
}
?>

<br><br>
<div class='row'>
	<div class='carousel-container'>
		<div class="carousel slide" id="myCarousel" data-ride="carousel">
			<ol class="carousel-indicators">
				<li class="active" data-target="#myCarousel" data-slide-to="0"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
			</ol>
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img class='cimg' alt="Meat Pizza" src="images/Pizza1.jpg">
				</div>
				<div class="carousel-caption">
						<h3>Satisfaction guaranteed. No refunds.</h3>
					</div>
				<div class="item">
					<img class='cimg' alt="Veggie Pizza" src="images/Pizza2.jpg">
					<div class="carousel-caption">
						<h3>Pizza</h3>
					</div>
				</div>
			</div>
		</div>
		<a class="left carousel-control" role="button" href="#myCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" role="button" href="#myCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>

<?php include 'footer.php'; ?>
