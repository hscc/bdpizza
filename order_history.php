<!DOCTYPE>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<?php
	session_start();
	include 'conn.php';
	include 'header.php';
?>
<body>
<?php
	//needs to be integrated with the rest of the program
	$customer_id = $_SESSION['user'];
	
	$query = "SELECT c.customer_order_id, c.order_date 
		FROM customer_order AS c 
		WHERE (c.customer_id = '$customer_id')
		AND (c.incart = 'n');";
	$result = mysqli_query($con, $query);
?>
	<hr>
	<h2>Orders</h2>
	<br><br>
	<!-- the table that lists the orders that are not incart -->
	<table style="margin: 0px auto;" class="table table-hover">
		<thead>
			<tr>
				<th>Order ID</th>
				<th>Order Date</th>
			</tr>
		</thead>
		<?php
			while ($row = mysqli_fetch_array($result)) {
				$order_id = $row['customer_order_id'];
				$order_date = $row['order_date'];
				
				echo "<tr>";
				echo "<td>$order_id</td>";
				echo "<td>$order_date</td>";
				echo "<td> <a href='order_history_details.php?order_id=$order_id' class='btn btn-info btnmd'>Order Details</a> </td>";
				echo "</tr>";
			}
		?>
	</table>

<?php include 'footer.php';  ?>
</body>
</html>
