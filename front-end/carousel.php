 <div class='row'>
        <div class='carousel-container'>
        <div class="carousel slide" id="myCarousel" data-ride="carousel">
            <ol class="carousel-indicators">
                <li class="active" data-target="#myCarousel" data-slide-to="0"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img class='cimg' alt="Meat Pizza" src="Pizza1.jpg">
                </div>
                <div class="item">
                    <img class='cimg' alt="Veggie Pizza" src="Pizza2.jpg">
                    <div class="carousel-caption">
                        <h3>Pizza</h3>

                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" role="button" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" role="button" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
        </div>