function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function CheckPassword() {
    var pass = /(?=^.{8,15}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    var password = document.getElementById("password");
    if (password.value.match(pass)) {
        alert('Password is Valid.')
        return true;
    } else {
        alert('Password is invalid. Please try again')
        return false;
    }
}

