 <div class="row">
            <div id='footer'>
            <h3 class="footertext">Contact BDPizza Today:</h3>
            <br>
            <div class="col-md-3">
                <center>
                    <img src="Facebook.jpg" class="img-circle" alt="About Us Logo" width="20%">
                    <br>
                    <h4 class="footertext">Facebook</h4>
                    <p class="footertext">
                        <font color="white"></font>Like us on Facebook today!<br>
                </center>
            </div>
            <div class="col-md-3">
                <center>
                    <i class='glyphicon glyphicon-facebook'></i>
                    <a href='Instagram.html'>
                        <img src="instagram.png" class="img-circle" alt="Instagram Logo" width="20%">
                    </a>
                    <br>
                    <h4 class="footertext">Follow Us on Instagram!</h4>
                    <p class="footertext">Follow us on instagram and stay notified on updates.<br>
                </center>
            </div>
            <div class="col-md-3">
                <center>
                    <a href='twitter.html'>
                        <img src="twitter.png" class="img-circle" alt="Twitter" width="20%">
                    </a>
                    <br>
                    <h4 class="footertext">Follow Us on Twitter!</h4>
                    <p class="footertext">Don't miss out on weekly announcements<br>
                </center>
            </div>
            <div class="col-md-3">
                <center>
                    <a href='YouTube.html'>
                        <img src="YouTube.jpg" class="img-circle" alt="Youtube" width="20%">
                    </a>
                    <br>
                    <h4 class="footertext">BDPizza Channel</h4>
                    <p class="footertext">Subscribe to our Youtube channel<br>
                </center>
            </div>
        </div>
    </div>
    <div class='row'>
        <center>
   
            <br>
            <h4 class="footertext">Legal Information</h4>
            <p id='legal'>® 2017 BDPizza, Inc. All Rights Reserved. BDPizza Name, Logos And Related Marks Are Trademarks Of BDPizza, Inc.<br>
                <p2 id='legal'> Offers good for a limited time at participating U.S. BDpizza restaurants. Prices may vary. Not valid with any other coupons or discounts. All beverage related trademarks are registered Trademarks of PepsiCo, Inc. Some offers require the purchase of multiple pizzas. Some offers may be available online only. No triple toppings or extra cheese. Certain toppings may be excluded from special offer pizzas or require additional charge. Additional toppings extra. Limit seven toppings to ensure bake quality. Limited delivery area. Delivery fee may apply and may not be subject to discount. Minimum purchase may be required for delivery. Customer responsible for all applicable taxes.
                    <br> Customer Care Team Contact Us @ BDPizza.com/customer-care or BDPizza@gmail.com</p2>
        </center>
    </div>
    </div>
</body>

</html>
