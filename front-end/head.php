<!DOCTYPE html>
<html lang="en">

<head>
    <title>Pizza Website</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' href=bdpizza.css>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='bdpizza.js'></script>
    <link rel=“stylesheet” href=“font-awesome/css/font-awesome.css” />
</head>

<body>
    <center>
    <img src="bdpizza.png">
    </center>
    <div class='container'>
        
        <div class='row'>
    <div class='header'>
        <nav class='navbar'>
            <div class="topnav" id="myTopnav">
                <a id='title' href="BDPizza.html" class='navbar-link'><strong>BDPizza</strong></a>
                <span class="divider"></span>
                <a href="menu.html" class='navbar-link'>Menu</a>
                <div class="divider"></div>
                <a href="order.html" class='navbar-link'>Order</a>
                <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
                <div class='row'>
                    <a class='sign-in' href='sign-in.html'>
                                Sign In
                         <span class='glyphicon glyphicon-save'></span>
                        </a>
                    <span class="divider"></span>
                    <a class='view-cart' href='cart.html'>
                                View Cart
                            <span class='glyphicon glyphicon-shopping-cart'></span>
                        </a>
                </div>
            </div>
        </nav>
    </div>
        </div>