<?php 
    session_start();
    include 'header.php';
    
    if(isset($_SESSION['menuadd'])) {
    	$menuadd = $_SESSION['menuadd'];
    	echo "<div class='alert alert-success' align='center'>";
    	echo "<b>$menuadd</b>";
    	echo "</div>";
    	unset($_SESSION['menuadd']);
    }
    
    echo "<hr>";
    echo "<h2>Menu</h2>";
    echo "<br><br>";

	echo "<form class='button' method='post' action='checkout.php'>";
	echo "<button class='btn btn-info btnmd' type='submit'>Go To Checkout</button>";
	echo "</form>";
	echo "<br>";
?>

<table style="margin: 0px auto;" class="table table-hover">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Size</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Add</th>
		</tr>
	</thead>
<?php
	include 'conn.php';
	$query = "SELECT p.product_id, p.product_name, p.size, p.price FROM product p, product_type pt where p.product_type_id = pt.product_type_id";
	$result = mysqli_query($con, $query);

	while ($row = mysqli_fetch_array($result)){
		$product_name = $row['product_name'];
		$size = $row['size'];
		$price = $row['price'];
		$product_id = $row['product_id'];
		
		echo "<form class='button' method='post' action='addToCart.php'>";
		echo "<tr>";
		echo "<td>$product_name</td>";
		echo "<td>$size</td>";
		echo "<td>$price</td>";	
		echo "<td><input type='number' class='form-control' name='qty'></td>";
		echo "<input type='hidden' name='product_id' value='$product_id'>";
		echo "<td><button class='btn btn-info btnmd' name='add' type='submit'>Add</button></td>";
		echo "</tr>";
		echo "</form>";
	}

	echo "</table>";
	
	include 'footer.php';
?>
</html>
