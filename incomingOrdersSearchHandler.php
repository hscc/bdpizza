<!DOCTYPE>
<html>

<body>
	<?php
		//set up
		include 'conn.php';
		$filter = $_GET["filter"];
	?>

	<hr>
	<h2>Incoming Orders</h2>
	<br><br>
	
	<!-- the table that lists the orders that are incart -->
	<table style="margin: 0px auto;" class="table table-hover">
		<thead>
			<tr>
				<th>Customer ID</th>
				<th>Order Details</th>
				<th>Delivery Method</th>
				<th>Order Date</th>
			</tr>
		</thead>
		
		<?php
			//gets information from the database
			$query = "SELECT c.customer_order_id, c.customer_id, c.delivery_method, c.order_date FROM customer_order AS c WHERE c.incart = 'y' AND c.customer_id LIKE '%$filter%';";
			$result = mysqli_query($con, $query);
			
			while ($row = mysqli_fetch_array($result)) {
				$order_id = $row['customer_order_id'];
				$cust_id = $row['customer_id'];
				$delivery_method = $row['delivery_method'];
				$order_date = $row['order_date'];
				
				//echo "<form class='button' method='post' action='orderUpdateHandler.php'>";
				echo "<tr>";
				echo "<td>$cust_id</td>";
				echo "<td> <a href='order_history_details.php?order_id=$order_id' class='btn btn-info btnmd'>Order Details</a> </td>";
				echo "<td>$delivery_method</td>";
				echo "<td>$order_date</td>";
				
				echo "<input type='hidden' name='customer_order_id' value='$order_id'>";
				
				//buttons that do different things
				//echo "<td><button class='btn btn-info btnmd' type='submit'>Update</button></td>";
				echo "<td><a href='delOrderHandler.php?order_id=$order_id' class='btn btn-danger btnmd'>Delete</a> </td>";
				
				echo "</tr>";
				//echo "</form>";
			}
			
			mysqli_close($con);
			
		?>
	</table>

</body>
</html>
