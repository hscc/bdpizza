<html>
    <head>
        <title>BDPizza</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="home.css">
		<link rel='stylesheet' href="bdpizza.css">
		<link rel="icon" href="images/favicon.ico"/>
		<header>

		<div align="center"><img src="images/bdpizza.png"></div>
            
			<h1 class="offscreen"></h1>
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navbar">
						<ul class="nav navbar-nav">
							<?php 
								if(isset($_SESSION['admin'])) {
									echo "<li><a href='home.php'>Home</a></li>";
									echo "<li><a href='createProduct.php'>Create Product</a></li>";
									echo "<li><a href='editProduct.php'>Edit Products</a></li>";
									echo "<li><a href='editProductType.php'>Edit Product Types</a></li>";
									echo "<li><a href='users.php'>Manage Users</a></li>";
									echo "<li><a href='incomingOrders.php'>Incoming Orders</a></li>";
									echo "</ul><ul class='nav navbar-nav navbar-right'><li><a href='signout.php'>
										<span class='glyphicon glyphicon-log-out'></span> Sign Out</a></li></ul>";
								}
								
								elseif(isset($_SESSION['loggedin'])) {
									echo "<li><a href='home.php'>Home</a></li>";
									echo "<li><a href='menu.php'>Menu</a></li>";
									echo "<li><a href='checkout.php'>Checkout</a></li>";
									echo "<li><a href='account.php'>Account</a></li>";
									echo "<li><a href='order_history.php'>Order History</a>";
									echo "</ul><ul class='nav navbar-nav navbar-right'><li><a href='signout.php'>
										<span class='glyphicon glyphicon-log-out'></span> Sign Out</a></li></ul>";
								}
								
								else {
									echo "<li><a href='index.php'>Login</a></li>";
									echo "<li><a href='signup.php'>Sign Up</a></li>";
								}
							?>
						</ul>
					</div>
				</div>
			</nav>
			</header>
    </head>
	<div class="content">
    	<div class="container-fluid">
    	<br><br>
