<?php
	session_start();
	include 'header.php';
	include 'conn.php';
	
	//if admin deleted a product
	if (isset($_GET['ptype_id'])) {
		$pt_id= $_GET['ptype_id'];
		
		$query = "DELETE FROM product_type WHERE product_type_id = '$pt_id'";
		mysqli_query($con, $query);
		
		//tells admin what happened after they clicked delete
		$pt_name = $_GET['ptype_name'];
		$_SESSION['EPT_message'] = "\"$pt_name\" has been deleted from product types.";
	}
	
	if($_SESSION['admin'] != true) {
		header("Location: index.php");
	}
	
	//tells admin what happend
	if(isset($_SESSION['EPT_message'])) {
		$EPT_message= $_SESSION['EPT_message'];
		echo "<div class='alert alert-success' align='center'>";
		echo "<b>$EPT_message</b>";
		echo "</div>";
		unset($_SESSION['EPT_message']);
	}
?>

<hr>
<h2>Edit Product Type</h2>
<br><br>
<table style="margin: 0px auto;" class="table table-hover">
	<thead>
		<tr>
			<th>Product Type Name</th>
		</tr>
	</thead>
	
<?php
	$query = "SELECT * FROM product_type";
	$result = mysqli_query($con, $query);
		
	while ($row = mysqli_fetch_array($result)){
		$ptype_name = $row['product_type_name'];
		$ptype_id = $row['product_type_id'];
		
		echo "<form class='button' method='post' action='prodUpdateHandler.php'>";
		echo "<tr>";
		echo "<td><input type='text' class='form-control' name='ptype_name' value='$ptype_name'></td>";
		echo "<input type='hidden' name='ptype_id' value='$ptype_id'>";
		echo "<td></td>";//extra space
		echo "<td></td>";//extra space
		echo "<td><button class='btn btn-info btnmd' type='submit'>Update</button></td>";
		echo "<td><a href='editProductType.php?ptype_id=$ptype_id&ptype_name=$ptype_name' class='btn btn-danger btnmd'>Delete</a></td>";
		echo "</tr>";
		echo "</form>";
	}
?>
</table>
<?php include 'footer.php'; ?>
</html>
