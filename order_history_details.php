<!DOCTYPE>
<html>
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
</head>

<?php
	session_start();
	include 'conn.php';
	include 'header.php';
?>
<body>

<?php
	$order_id = $_GET['order_id'];
	
	//same sql statement from checkout.php
	$query1 = "select sum(c.qty) as qty, c.product_id, p.product_name, p.size, p.price as price from customer_order_item c, product p where p.product_id = c.product_id and c.customer_order_id = $order_id group by c.product_id, p.product_name, p.size";
	$result1 = mysqli_query($con, $query1);
	
	
?>
	<hr>
	<h2>Orders Details</h2>
	<br><br>
	
	<!-- the table that lists the orders details of the specified order -->
	<table style="margin: 0px auto;" class="table table-hover">
		<thead>
		<tr>
			<th>Product Name</th>
			<th>Size</th>
			<!--  <th>Addons</th> -->
			<th>Quantity</th>
			<th>Price</th>
		</tr>
		</thead>
		<?php
			$total_price = 0;
		
			while ($row = mysqli_fetch_array($result1)) {
				//gets order details stuff
				$product_name = $row['product_name'];
				$size = $row['size'];
				$qty = $row['qty'];
				$price = $row['price'];
				//$CO_item_id = $row['customer_order_item_id'];
				
				/*
				//sets up the query for the addons stuff
				$query2 = "SELECT addons.addon_name, addons.price
							FROM customer_order_item_addon
							INNER JOIN addons ON addons.addon_id = customer_order_item_addon.addon_id
							WHERE customer_order_item_addon.customer_order_item_id = '$CO_item_id'
							AND customer_order_item_addon.customer_order_id = '$order_id';";
				$result2 = mysqli_query($con, $query2);
				*/
				
				//this starts the next row of the table
				echo "<tr>";
				echo "<td>$product_name</td>";
				echo "<td>$size</td>";
				
				/*
				//shows the addons of each order item
				echo "<td>";
				while ($row = mysqli_fetch_array($result2)) {
					$addon = $row['addon_name'];
					$addon_price = $row['price'];
					
					echo "$addon; ";
					$price += $addon_price;
				}
				*/
				
				//shows the quantity and final price
				echo "</td>";
				echo "<td>$qty</td>";
				
				$price = $price * $qty;
				$price = number_format($price, 2, '.', '');	//to make the price look like this: $9.00 instead of this: $9
				echo "<td>$$price</td>";
				
				$total_price += $price;
				
				echo "</tr>";
			}
			
			$total_price = number_format($total_price, 2, '.', '');
		?>
	</table>
	<h3 align="center">Total Price is <?php echo "$$total_price"; ?></h3>
		
<?php 
//for the bottom of the page
include 'footer.php'; 
?>
	
</body>
</html>
