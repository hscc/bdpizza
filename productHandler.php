<?php
//code to either create a new product or create a new product type

//Note: insert into statements only work when primary keys are auto incremented

//set up
include 'conn.php';
session_start();

if (isset($_POST['p-type-id'])) {	//if the admin is creating a new product
	$pname = $_POST['pname'];
	$size = $_POST['size'];
	$price = $_POST['price'];
	$pTypeID = $_POST['p-type-id'];

	if(($pname!="") && ($size!="") && ($price!="")) {	//add new product if all values were listed
		$query = "select product_name, size from product where product_name='$pname' and size='$size'";
		$result = mysqli_query($con, $query);
		$count = mysqli_num_rows($result);
		
		if($count == 0){
			$sql = "INSERT INTO `bdpizza`.`product` (`product_type_id`, `product_name`, `size`, `price`) VALUES ('$pTypeID', '$pname', '$size', '$price')";
			$result = mysqli_query($con, $sql);
			$_SESSION['CP_message'] = "<div class='alert alert-success alert-dissmisable'>Added new product.</div>";
		}
		else {
			$_SESSION['CP_message'] = "<div class='alert alert-danger alert-dissmisable'>There is already a product with that name and size.</div>";
		}
	}
	else {	//if any of the values aren't typed in, tell admin to retype the values
		$_SESSION['CP_message'] = "<div class='alert alert-danger alert-dissmisable'>Please type in all of the inputs listed.</div>";
	}
}
elseif(isset($_POST['ptype_name'])) {	//if the admin is createing a new product type
	$ptype_name = $_POST['ptype_name'];
	
	if ($ptype_name!=null) {	//add new product type in the value is not empty
		$query = "select product_type_name from product_type where product_type_name='$ptype_name'";
		$result = mysqli_query($con, $query);
		$count = mysqli_num_rows($result);
		//auto incrememnt the prod type id and remove the variable
		if($count == 0){
			$sql = "INSERT INTO `bdpizza`.`product_type` (`product_type_name`) VALUES ('$ptype_name');";
			$result = mysqli_query($con, $sql);
			$_SESSION['CP_message'] = "<div class='alert alert-success alert-dissmisable'>Addded new product type.</div>";
		}
		else {
			$_SESSION['CP_message'] = "<div class='alert alert-danger alert-dissmisable'>There is already a product type with that name.</div>";
		}
	}
	else {	//if any of the values aren't typed in, tell admin to retype the values
		$_SESSION['CP_message'] = "<div class='alert alert-danger alert-dissmisable'>Please type in all of the inputs listed.</div>";
	}
}
else { //if neither two options above happened or something went wrong
	$_SESSION['CP_message'] = "<div class='alert alert-danger alert-dissmisable'>An unexpected error occurred.</div>";
}

//go back to the previous page
header('Location: createProduct.php');