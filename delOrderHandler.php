<?php
	include 'conn.php';
	
	session_start();
	$order_id = $_GET['order_id'];	//gets the order that needs to be deleted
	
	//deletes the order in the data base tables
	$query = "DELETE FROM customer_order WHERE customer_order_id='$order_id'";
	mysqli_query($con, $query);
	
	$query = "DELETE FROM customer_order_item WHERE customer_order_id='$order_id'";
	mysqli_query($con, $query);
	
	$query = "DELETE FROM customer_order_item_addon WHERE customer_order_id='$order_id'";
	mysqli_query($con, $query);
	
	//the message when a order is deleted
	session_start();
	$_SESSION['IO_message'] = "Deleted order.";
	
	//go back to the previous page
	header("Location: incomingOrders.php");
?>