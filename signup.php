<?php
	session_start();
	include 'header.php';
	include 'conn.php';
	$_SESSION['re1'] = rand(1, 1000000);
?>

<div align="center">
    <?php 
        if (isset($_SESSION['empty'])) {
			echo $_SESSION['empty'];
        	$_SESSION['empty']='';
        }
        
        if (isset($_SESSION['usererror'])) {
			echo "<div class='alert alert-danger'>" . $_SESSION['usererror'] . "</div>";
			$_SESSION['usererror']='';
			unset($_SESSION['usererror']);
        }
        
        if (isset($_SESSION['passerror'])) {
			echo "<div class='alert alert-danger'>" . $_SESSION['passerror'] . "</div>";
			$_SESSION['passerror']='';
			unset($_SESSION['passerror']);
        }
    
        if (isset($_SESSION['recaperror'])) {
			echo "<div class='alert alert-danger'>" . $_SESSION['recaperror'] . "</div>";
			$_SESSION['recaperror']='';
			unset($_SESSION['recaperror']);
		}
    ?>
</div>

<hr>
<h2>Sign Up</h2>
<br><br>
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<form action="signupHandler.php" method=POST>
			<div class="form-group" align="left">
				<label for="user">Username:</label>
				<input type="text" class="form-control" name="user"   placeholder="Enter username" required>
			</div>
			<div class="form-group" align="left">
				<label for="pass">Password:</label>
				<input type="password" class="form-control" name="pass" minlength="8" maxlength="20"  placeholder="Enter password" required>
			</div>
			<div class="form-group" align="left">
				<label for="pass">Confirm Password:</label>
				<input type="password" class="form-control" name="conpass" minlength="8" maxlength="20"  placeholder="Retype password" required>
			</div>
			<div class="form-group" align="left">
				<label for="recap">ReCAPTCHA&#0153;:</label>
				<input type="number" class="form-control" name="recap" placeholder="Type the number: <?php echo $_SESSION['re1']?>" required>
			</div>
			<button type="submit" class="btn btn-warning btn-block">Sign Up</button>
		</form>
	</div>
	<div class="col-sm-4"></div>
</div>

<?php include 'footer.php'; ?>
