<?php
include 'conn.php';
session_start();

if (isset($_SESSION['admin'])) { //get the user specified in the url if the user is the admin
	$user = $_GET['user'];
}
else { //get the customer_id of the user
	$user = $_SESSION['user'];
}


if (isset($_POST['realpass'])){
	$oldpass = $_POST['oldpass'];
	$correctpass = $_POST['realpass'];
	$password = $_POST['pass'];
	$conpass = $_POST['conpass'];
	$length = strlen($password);
	if(($oldpass == $correctpass) && ($password == $conpass) && ($length>=8)){
		$sql = "UPDATE `bdpizza`.`customer` SET `password`='$password' WHERE `customer_id`='$user'";
		$result = mysqli_query($con, $sql);
		$password = null;
		$_SESSION['message'] = "Password Updated.";
	}
	header("Location: account.php?user=$user");
}

elseif (isset($_POST['active'])){	 
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$state = $_POST['state'];
	$zip = $_POST['zip'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$sql = "UPDATE `bdpizza`.`customer` SET `first_name`='$fname', `last_name`='$lname', `address`='$address', `city`='$city', `state`='$state', `zip`='$zip', `phone`='$phone', `email`='$email' WHERE `customer_id`='$user'";
	$result = mysqli_query($con, $sql);
	$active = null;
	$_SESSION['message'] = "Account Updated.";
	header("Location: account.php?user=$user"); 
} 

else{
	$_SESSION['message'] = "Error";
	header("Location: account.php?user=$user"); 		
}