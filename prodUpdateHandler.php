<?php
include 'conn.php';
session_start();

if (isset($_POST['product_id'])) {	//if admin updated a product
	$pname = $_POST['product_name'];
	$size = $_POST['size'];
	$price = $_POST['price'];
	$prod_type_id = $_POST['prod_type_id'];
	$product_id = $_POST['product_id'];
	
	if(($pname!=null) && ($size!=null) && ($price!=null) && ($prod_type_id!=null)){
		$sql = "UPDATE `product` SET `product_name`='$pname', `size`='$size', `price`='$price', `product_type_id`='$prod_type_id' WHERE `product_id`='$product_id'";
		$result = mysqli_query($con, $sql);
		
		$pname = null;
		$size = null;
		$price = null;
		$prod_type_id = null;
		$product_id = null;
	}
	
	$_SESSION['EP_message'] = "Updated product.";
	header('Location: editProduct.php');
}
elseif (isset($_POST['ptype_id'])) {	//if admin updated a product type 
	$ptype_name = $_POST['ptype_name'];
	$ptype_id = $_POST['ptype_id'];
	
	if (($ptype_name!=null)){
		$sql = "UPDATE `bdpizza`.`product_type` SET `product_type_name`='$ptype_name' WHERE `product_type_id`='$ptype_id'";
		$result = mysqli_query($con, $sql);
		
		$ptype_name = null;
		$ptype_id = null;
		$oldptype_id = null;
	}
	
	$_SESSION['EPT_message'] = "Updated product type.";
	header('Location: editProductType.php');
}
else{	//if something went wrong
	echo "it did not work";
	header('Location: home.php'); 
}