<?php
	session_start();
	include 'header.php';
	include 'conn.php';
?>
<?php
	if (isset($_SESSION['order_id'])) {
		$order_id = $_SESSION['order_id'];
	}
	//if user deleted an item
	if (isset($_GET['product_id'])) {
		$prod_id= $_GET['product_id'];
		
		$query = "DELETE FROM customer_order_item WHERE product_id = '$prod_id' AND customer_order_id = '$order_id';";
		mysqli_query($con, $query);
		
		$prod_name = $_GET['product_name'];
		$sz = $_GET['sz'];
		echo "<div class='alert alert-success' align='center'>";
		echo "<b>'$sz $prod_name' has been deleted from your cart.<b>"; //tells admin what happened after they clicked delete
		echo "</div>";
	}
	
	echo "<hr>";
	echo "<h2>Checkout</h2>";
	echo "<br><br>";
	
	echo "<table class='table table-hover'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th>Product Name</th>";
	echo "<th>Size</th>";
	echo "<th>Quantity</th>";
	echo "<th>Price</th>";
	echo "</tr>";
	echo "</thead>";

	if (isset($_SESSION['order_id'])) {
		$query = "select sum(c.qty) as qty, c.product_id, p.product_name, p.size, p.price as price from customer_order_item c, product p where p.product_id = c.product_id and c.customer_order_id = $order_id group by c.product_id, p.product_name, p.size";
		$result = mysqli_query($con, $query);
		$ptotal = 0;
		$_SESSION['productnum'] = 0;
		
		while($row=mysqli_fetch_array($result)) {
			$qty = $row['qty'];
			$pname = $row['product_name'];
			$size = $row['size'];
			$price = $row['price'];
			$product_id = $row['product_id'];
			
			echo "<tr>";
			echo "<td>$pname</td>";
			echo "<td>$size</td>";
			echo "<td>$qty</td>";
			$total = $price * $qty;
			$ptotal += $total;
			
			$total= number_format($total, 2, '.', '');
			echo "<td>$$total</td>";
			
			echo "<td><a href='checkout.php?product_id=$product_id&product_name=$pname&sz=$size&' class='btn btn-danger btnmd'>Delete</a></td>";
			echo "</tr>";
			
			$_SESSION['productnum'] = $_SESSION['productnum'] + 1;
		}
		
		echo "</table>";
		echo "<br>";
		echo "<table><tr><td>";

		if($_SESSION['productnum'] == 0) {
			echo "<form class='button' method='post' action='menu.php'>";
			echo "<button class='btn btn-danger btnmd' type='submit'>Please Place An Order</button>";
			echo "</form></td></tr></table>";
		}
		else {
			echo "</table>";
			echo "<div class='row'>";
			echo "<div class='col-sm-4'></div>";
			echo "<div class='col-sm-2'>";
			echo "<form class='button' method='post' action='payment.php'>";
			echo "<button class='btn btn-info btnmd' type='submit'>Pay and Continue</button>";
			echo "</form>";
			echo "</div>";

			echo "<div class='col-sm-2'>";	
			echo "<td><form class='button' method='post' action='menu.php'>";
			echo "<button class='btn btn-info btnmd' type='submit'>Continue Shopping</button>";
			echo "</form>";
			echo "</div>";

			echo "<div class='col-sm-4'></div></div>";
			echo "<br>";
			
			$ptotal = number_format($ptotal, 2, '.', '');
			echo "<div align='center'><h3>Total Price is $" . $ptotal . "</h3></div>";
			//Saves as Session var for paymentHandler.php
			$_SESSION['ptotal'] = $ptotal;
		}
	}
	
	else {
		echo "</table><br><table><tr><td>";
		echo "<form class='button' method='post' action='menu.php'>";
		echo "<button class='btn btn-danger btnmd' type='submit'>Please Place An Order</button>";
		echo "</form></td></tr></table>";
	}



include 'footer.php';
?>
