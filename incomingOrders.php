<!DOCTYPE>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script type="text/javascript">
		//the ajax function to change the table
		function listUsers(str) {
			//if there is nothing in the search bar, end the function and don't display the table
			if (str == "") {
				document.getElementById("text").innerHTML = "<br><hr><h2>Search for customers with the search bar above.</h2>";
				return;
			}
			else {
				xmlhttp = new XMLHttpRequest(); //get new request

				//when the search bar changes value, change the "text" element to this.reponseText,
				//which is the html in incomingOrdersSearchHandler.php
				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						document.getElementById("text").innerHTML = this.responseText;
					}
				}
			}

			//sends the request to usersSearchHandler
			xmlhttp.open("GET", "incomingOrdersSearchHandler.php?filter=" + str, true);
			xmlhttp.send();
		}
	</script>
</head>

<?php
	session_start();
	include 'conn.php';
	include 'header.php';
?>
<body>
	<?php
		if($_SESSION['admin'] != true) {
			header("Location: index.php");
		}
		
		if(isset($_SESSION['IO_message'])) {
			$IO_message= $_SESSION['IO_message'];
			echo "<div class='alert alert-success' align='center'>";
			echo "<b>$IO_message</b>";
			echo "</div>";
			unset($_SESSION['IO_message']);
		}
	?>
	
	<hr>
	
	<!--search bar -->
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<form>
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
				<input class="form-control" onkeyup="listUsers(this.value)" type="text" name="searchBar" placeholder="Search for users">
			</div>
		</form>
	</div>
	<div class="col-sm-4"></div>
		
	<br><br><br>
	
	<!-- the thing that's going to change into a table when the search bar is used -->
	<div id="text"><br><h2>Search for incoming orders with the search bar above.</h2></div>

	<?php include 'footer.php'; ?>

</body>
</html>
