<?php
	session_start();
	include 'header.php';
	include 'conn.php';
	
	if(isset($_SESSION['message'])){
		if($_SESSION['message']== "Payment Information Updated") {
			echo "<div class='alert alert-success' align='center'>";
			echo "Payment Information Updated!</div>";
		}
		
		else {
			echo "<div class='alert alert-danger' align='center'An unexpected error has occurred.</div>";
		}
		//resets message
		$_SESSION['message'] = null;
	}
	
	?> <hr> <?php 

	$user = $_SESSION['user'];
	$query = "SELECT co.delivery_method, co.payment_method, co.credit_card_number, c.address, c.city, c.state, c.zip, c.phone, c.email FROM customer_order co, customer c where co.customer_id = c.customer_id and c.customer_id = '$user'";
	$result = mysqli_query($con, $query);

	while ($row = mysqli_fetch_array($result)){
		$delivery_method = $row['delivery_method'];
		$payment_method = $row['payment_method'];
		$credit_card_number = $row['credit_card_number'];
		$address = $row['address'];
		$city = $row['city'];
		$state = $row['state'];
		$zip = $row['zip'];
		$phone = $row['phone'];
		$email = $row['email'];
	}

	echo "<h2>Payment Information</h2>";
	echo "<div class='col-sm-3'></div>";
	echo "<div class='col-sm-6'>";
	echo "<br><br>";
	echo "<form method='post' action='paymentHandler.php'>";
	echo "<div align='left'>";
	
	//delivery select input
	echo "Delivery Method: <select type='text' class='form-control' name='delivery_method' value='$delivery_method'>";
	if ($delivery_method == 'delivery') {
		echo "<option class='form-control' value='$delivery_method' selected='selected'>$delivery_method</option>"; 
		echo "<option class='form-control' value='carry out'>carry out</option>"; 
	}
	elseif  ($delivery_method == 'carry out') {
		echo "<option class='form-control' value='delivery' >delivery</option>";
		echo "<option class='form-control' value='$delivery_method' selected='selected'>$delivery_method</option>"; 
	}
	else {
		echo "<option class='form-control' value='delivery'>delivery</option>";
		echo "<option class='form-control' value='carry out'>carry out</option>"; 
	}
	echo "<select> <br>";
	
	//payment select input
	echo "Payment Method: <select type='text' class='form-control' name='payment_method' value='$payment_method'>";
	if ($payment_method== 'Cash') {
		echo "<option class='form-control' value='$payment_method' selected='selected'>$payment_method</option>";
		echo "<option class='form-control' value='Discovery'>Discovery</option>";
		echo "<option class='form-control' value='Mastercard'>Mastercard</option>";
		echo "<option class='form-control' value='Visa'>Visa</option>";
	}
	elseif  ($payment_method== 'Discovery') {
		echo "<option class='form-control' value='Cash'>Cash</option>";
		echo "<option class='form-control' value='$payment_method' selected='selected'>$payment_method</option>";
		echo "<option class='form-control' value='Mastercard'>Mastercard</option>";
		echo "<option class='form-control' value='Visa'>Visa</option>";
	}
	elseif  ($payment_method== 'Mastercard') {
		echo "<option class='form-control' value='Cash'>Cash</option>";
		echo "<option class='form-control' value='Discovery'>Discovery</option>";
		echo "<option class='form-control' value='$payment_method' selected='selected'>$payment_method</option>";
		echo "<option class='form-control' value='Visa'>Visa</option>";
	}
	elseif  ($payment_method== 'Visa') {
		echo "<option class='form-control' value='Cash'>Cash</option>";
		echo "<option class='form-control' value='Discovery'>Discovery</option>";
		echo "<option class='form-control' value='Mastercard'>Mastercard</option>";
		echo "<option class='form-control' value='$payment_method' selected='selected'>$payment_method</option>";
	}
	else {
		echo "<option class='form-control' value='Cash'>Cash</option>";
		echo "<option class='form-control' value='Discovery'>Discovery</option>";
		echo "<option class='form-control' value='Mastercard'>Mastercard</option>";
		echo "<option class='form-control' value='Visa'>Visa</option>";
	}
	echo "<select> <br>";
	
	echo "Credit Card #: <input type='text' class='form-control' name='credit_card_number' value='$credit_card_number'><br>";
	echo "Address: <input type='text' class='form-control' name='address' value='$address'><br>";
	echo "City: <input type='text' class='form-control' name='city' value='$city'><br>";
	echo "State: <input type='text' class='form-control' name='state' value='$state'><br>";
	echo "Zip: <input type='text' class='form-control' name='zip' value='$zip'><br>";
	echo "Phone: <input type='text' class='form-control' name='phone' value='$phone'><br>";
	echo "Email: <input type='text' class='form-control' name='email' value='$email'><br>";
	echo "<input type='hidden' name='active' value='active'>";
	echo "<button class='btn btn-info btnmd' type='submit'>Update Account</button>";
	echo "</form>";
	echo "</div>";
	echo "</div>";
	echo "<div class='col-sm-3'></div>";

	include 'footer.php';
?>
