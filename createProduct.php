<?php
	session_start();
	include 'header.php';
	include 'conn.php';
?>
<body>
<?php
	if($_SESSION['admin'] != true) {
		header("Location: index.php");
	}
	
	//tells admin what happened after they clicked submit
	if(isset($_SESSION['CP_message'])) {
		$CP_message= $_SESSION['CP_message'];
		echo "<div align='center'>";
		echo "<b>$CP_message</b>";
		echo "</div>";
		unset($_SESSION['CP_message']);
	}
?>

<div class="row">
	<div class="col-sm-6">
		<hr>
		<h2>Create Product</h2>
		<br><br>
		<form action="productHandler.php" method=POST>
			<div align="left">
				<div class="form-group">
					<label for="pname">Product Name:</label>
					<input type="text" class="form-control" name="pname" placeholder="Enter product name">
				</div>
				<div class="form-group">
					<label for="size">Size:</label>
					<input type="text" class="form-control" name="size" placeholder="Enter size">
				</div>
				<div class="form-group">
					<label for="price">Price:</label>
					<input type="number" step="0.01" class="form-control" name="price" placeholder="Enter price">
				</div>
				<div class="form-group">
					<label for="p-type-id">Product Type Name:</label>
					<select class="form-control" name="p-type-id">
						<?php 
						$query = "select product_type_name, product_type_id from product_type";
						$result = mysqli_query($con, $query);
						while($row = mysqli_fetch_array($result)){
							?> 
					<option value=<?php echo $row['product_type_id']?>> 
						<?php echo $row['product_type_name'];
							?>
					</option> 
						<?php }	?>
					</select>
				</div>
				<button type="submit" class="btn btn-info btnmd">Submit New Entry</button>
			</div>
		</form>
	</div>
	
	
	<div class="col-sm-6">
		<hr>
		<h2>Create Product Type</h2>
		<br><br>
		<form action="productHandler.php" method=POST>
			<div align="left">
				<div class="form-group">
					<label for="ptype_name">Product Type Name:</label>
					<input type="text" class="form-control" name="ptype_name" placeholder="Enter product type name">
				</div>
				<button type="submit" class="btn btn-info btnmd">Submit New Entry</button>
			</div>
		</form>
	</div>
</div>

<?php 
	include 'footer.php';
?>
