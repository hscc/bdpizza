<?php
	session_start();
	include 'header.php';
	include 'conn.php';
?>
<body>
<?php
	$user = "";
	if (isset($_SESSION['admin'])) { //get the user specified in the url if the user is the admin
		$user = $_GET['user'];
	}
	else { //get the customer_id of the user
		$user = $_SESSION['user'];
	}
	
	$query = "select customer_id, password, first_name, last_name, address, city, state, zip, phone, email from customer where customer_id = '$user'";
	$result = mysqli_query($con, $query);

	while ($row = mysqli_fetch_array($result)){
		$cust_id = $row['customer_id'];
		$password = $row['password'];
		$fname = $row['first_name'];
		$lname = $row['last_name'];
		$address = $row['address'];
		$city = $row['city'];
		$state = $row['state'];
		$zip = $row['zip'];
		$phone = $row['phone'];
		$email = $row['email'];
	}
	
	//welcome either the admin or the user
	if (isset($_SESSION['admin'])){
		echo "<div align='center'>";
		echo "<h2>Welcome Master Bruce.</h2><br>";
		echo "</div>";
	}
	else {
		echo "<div align='center'>";
		echo "<h2>Welcome " . $_SESSION['user'] . ".</h2><br>";
		echo "</div>";
	}

	if(isset($_SESSION['message'])) {
		$message= $_SESSION['message'];
		echo "<div class='alert alert-success' align='center'>";
		echo "<b>$message</b>";
		echo "</div>";
		unset($_SESSION['message']);
	}
?>

<div class="row">
	<div class="col-sm-6">
		<hr>
		<h2>Update Account</h2>
		<br><br>
		<form action="<?php echo "accountHandler.php?user=$user"?>" method=POST>
			<div align="left">
				<div class="form-group">
					<label for="user">Username:</label>
					<input type="text" class="form-control" name="user" value="<?php echo $cust_id; ?>" disabled>
				</div>
				<div class="form-group">
					<label for="oldpass">Current Password:</label>
					<input type="password" class="form-control" name="oldpass" placeholder="Enter current password">
				</div>
				<div class="form-group">
					<label for="pass">New Password:</label>
					<input type="password" class="form-control" name="pass" placeholder="Enter new password">
				</div>
				<div class="form-group">
					<label for="conpass">Confirm New Password:</label>
					<input type="password" class="form-control" name="conpass" placeholder="Retype new password">
				</div>
				<input type="hidden" name="realpass" value="<?php echo $password; ?>">
				<button type="submit" class="btn btn-info btnmd">Update Account</button>
				<button class="btn btn-danger btnmd" formaction="delUserHandler.php">Delete Account</button>
			</div>
		</form>
	</div>
	<div class="col-sm-6">
		<hr>
		<h2>Update Profile</h2>
		<br><br>
		<form action="<?php echo "accountHandler.php?user=$user"?>" method=POST>
			<div align="left">
				<div class="form-group">
					<label for="fname">First Name:</label>
					<input type="text" class="form-control" name="fname" value="<?php echo $fname; ?>">
				</div>
				<div class="form-group">
					<label for="lname">Last Name:</label>
					<input type="text" class="form-control" name="lname" value="<?php echo $lname; ?>">
				</div>
				<div class="form-group">
					<label for="address">Address:</label>
					<input type="text" class="form-control" name="address" value="<?php echo $address; ?>">
				</div>
				<div class="form-group">
					<label for="city">City:</label>
					<input type="text" class="form-control" name="city" value="<?php echo $city; ?>">
				</div>
				<div class="form-group">
					<label for="state">State:</label>
					<input type="text" class="form-control" name="state" value="<?php echo $state; ?>">
				</div>
				<div class="form-group">
					<label for="zip">Zip Code:</label>
					<input type="text" class="form-control" name="zip" value="<?php echo $zip; ?>">
				</div>
				<div class="form-group">
					<label for="phone">Phone Number:</label>
					<input type="tel" class="form-control" name="phone" value="<?php echo $phone; ?>">
				</div>
				<div class="form-group">
					<label for="email">Email:</label>
					<input type="email" class="form-control" name="email" value="<?php echo $email; ?>">
				</div>
				<input type="hidden" name="active" value="active">
				<button type="submit" class="btn btn-info btnmd">Update Profile</button>
			</div>
		</form>
	</div>
</div>

<?php include 'footer.php'; ?>
